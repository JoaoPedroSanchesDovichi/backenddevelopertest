# Border Router

Simple API project with one endpoint:

`GET /routing/origin/destination`
Source for origins and destinations available on https://raw.githubusercontent.com/mledoze/countries/master/countries.json

Upon acessing the link, the system will determine the shortest route from origin to destination using Dijkstra's algorithm.

Upon sucessfull calculation: the endpoint will return the following json object:

`{ "route" : ["point1", "point2", "point3", "point...", "pointN"] }`

Upon failure to process a route the endpoint will return HTTP error 400.

# Building and Running

This project uses maven software project and comprehension tool.

`mvn install` -> To build the project.

`mvn spring-boot:run` -> To execute the project.

`mvn clean` -> To clean the project folder from previous builds.

# Example

`localhost:8080/routing/CZE/ITA`

Expected result:

`{ route: ["CZE", "AUT", "ITA"] }`
