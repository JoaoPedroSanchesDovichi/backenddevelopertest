package jsanchesdovichi.border_router;

import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

import org.jgrapht.*;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm.*;
import org.springframework.http.ResponseEntity;

import Respostas.Rota;

@RestController
@SpringBootApplication
public class BorderRouterApplication {

    @GetMapping("/routing/{origin}/{destination}")
    ResponseEntity<Rota> router(@PathVariable final String origin, @PathVariable final String destination) {
        if (GraphSingleton.getInstance().graph.containsVertex(origin) && GraphSingleton.getInstance().graph.containsVertex(destination)) {

            DijkstraShortestPath<String, DefaultEdge> dijkstra = new DijkstraShortestPath<>(GraphSingleton.getInstance().graph);

            SingleSourcePaths<String, DefaultEdge> caminhos = dijkstra.getPaths(origin);

            if (caminhos != null) {

                GraphPath<String, DefaultEdge> caminho_final = caminhos.getPath(destination);

                if (caminho_final != null) {
                    if (caminho_final.getVertexList().isEmpty() == false) {
                        Rota resposta = new Rota();
                        resposta.setRoute(caminho_final.getVertexList());

                        return new ResponseEntity<>(resposta, HttpStatus.OK);
                    }
                }
            }
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    public static void main(String[] args) {
        GraphSingleton.getInstance();

        SpringApplication.run(BorderRouterApplication.class, args);
    }

}
