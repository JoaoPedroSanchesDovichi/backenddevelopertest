package jsanchesdovichi.border_router;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import Utils.Utilidades;

public class GraphSingleton {

    private static GraphSingleton instance = null;

    public final DefaultDirectedGraph<String, DefaultEdge> graph;

    public static GraphSingleton getInstance() {
        if (instance == null) {
            try {
                instance = new GraphSingleton();
            } catch (IOException ex) {
                Logger.getLogger(GraphSingleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return instance;
    }

    private void CriarGrafo() {
        try {
            Logger.getGlobal().log(Level.INFO, "Buscando dados json...");

            String resultado = Utilidades.getText("https://raw.githubusercontent.com/mledoze/countries/master/countries.json");

            Logger.getGlobal().log(Level.INFO, "Gerando grafo com as informações recebidas...");

            JsonElement parsed = JsonParser.parseString(resultado);

            JsonArray countries_list = parsed.getAsJsonArray();

            for (JsonElement country : countries_list) {
                JsonObject converted_country = country.getAsJsonObject();

                String origin_vertex = converted_country.get("cca3").getAsString();

                if (graph.containsVertex(origin_vertex) == false) {
                    graph.addVertex(origin_vertex);
                }

                JsonArray origin_borders = converted_country.getAsJsonArray("borders");

                for (JsonElement border : origin_borders) {
                    String adjacent_border = border.getAsString();

                    if (graph.containsVertex(adjacent_border) == false) {
                        graph.addVertex(adjacent_border);
                    }

                    graph.addEdge(origin_vertex, adjacent_border);
                }
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Erro ao recuperar objeto json com a URL fornecida");
        }
    }

    private GraphSingleton() throws MalformedURLException, IOException {

        graph = new DefaultDirectedGraph<>(DefaultEdge.class);

        CriarGrafo();
    }
}
